## Summary
[Kibana](https://jaws-kibana.jgi.doe.gov/app/dev_tools#/console) console

#### ElasticSearch
This repo contains two python scripts that expose the elasticsearch metrics.  The shell script source the python venv environment and the python script runs a server that queries elasticsearch with the API and serves the prometheus metrics with the "instrumenting" code, (see [prometheus docs](https://prometheus.io/docs/instrumenting/clientlibs/))
```
elastic_query_api.py
elastic_query_api.sh
```

#### Systemd Unit Files
There are also the systemd files which will also be moved to another repo at some point. They should live under `jaws-vm-1.jgi.lbl.gov:/etc/systemd/system`

```
elastic-exporter.service
elastic-query-api.service
```

Start the services on jaws-vm-1.jgi.lbl.gov
```
sudo su
cd /etc/systemd/system/
 1002  ls
systemctl start elastic-exporter.service
systemctl start elastic-query-api.service
```

### Manually Manipulating elasticSearch
This only works for elasticsearch on jaws-vm-2 for now.  The password is saved as a variable in CI/CD settings.

#### The ENV
```
# create venv for elasticsearch exporter
python3 -m venv env
. ./env/bin/activate
pip3 install -r requirements.txt
```

#### Commands
This only works for elasticsearch on jaws-vm-2 for now
```
# Get password from "Variables" under CI/CD tab in repository.
export ELASTIC_PASSWORD="<password>"
```

#### The curl command
this is used for connecting to jaws-vm-2 elasticsearch db.
create curl script that has
```
#!/bin/bash
sudo /usr/bin/curl \
        --cacert /etc/elasticsearch/certs/http_ca.crt \
        --user elastic:$ELASTIC_PASSWORD \
        --header "Content-Type: application/json"\
	"$@"
```

#### then you can do
```
./curl -XPUT https://localhost:9200/movies -d '
{
  "mappings": {
      "properties": {
        "year": {
	  "type": "date"
        }
      }
   }
}'

./curl -XGET https://localhost:9200/movies/_mapping?pretty

./curl -XPUT https://localhost:9200/movies/_doc/1?pretty -d '
{
  "genre": ["IMAX","SCI-FI"],
  "title": "Interstellar",
  "year": 2014
}'

./curl -XDELETE https://localhost:9200/movies
```

#### To do
Create a Docker container to run the exporter in the same mold as other jaws-services using systemd or supervisord.
