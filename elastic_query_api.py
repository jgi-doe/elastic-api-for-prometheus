#!/usr/bin/env python

"""
JAWS Daemon process periodically queries ElasticSearch
that will be used by the ElasticSearch exporter to transform them
into metrics usable by Prometheus.

This command will print out the metrics offered by this server
curl -XGET localhost:8000 | jq
"""

import os
import logging
from elasticsearch import Elasticsearch
import http.server
from datetime import datetime
import json
from logging_config import setup_logging
from datetime import datetime
from prometheus_client import start_http_server, Gauge

# add the metric name and a help message
REQUEST_INPROGRESS = Gauge('app_requests_inprogress', 'number of application requests in progress')

APP_PORT = 8000
METRICS_PORT=8001

# Setup logging for script 1
# If this app is run by root (i.e. via systemctl) then the log file
# will be saved as /root/elastic_query.log')
log_path = "/root/logs/elastic_query.log" # when root (systemd) is running this
#log_path = "elastic_query.log"           # when user is running this
logger = setup_logging(log_path)
if not os.path.exists(log_path):
    os.makedirs(log_path) 

logger.info(f"Starting elasticsearch monitoring daemon at {datetime.now()}")


def _search_elastic_search(
    host, port, api_key, index, query, aggregations=None
):  # noqa
    """
    Search Elastic Search (ES) DB

    :param elastic_client: ES Client
    :type elastic_client: obj
    :param query: ES top-level query
    :type query: dict
    :param aggregations: ES bucket aggregations
    :type aggregations: dict
    :return: ES search response
    :rtype: dict
    """
    response = None
    try:
        elastic_client = Elasticsearch(
            [f"http://{host}:{port}"], api_key=api_key
        )  # noqa
        response = elastic_client.search(
            index=index, query=query, aggregations=aggregations, size=10000
        )
    except Exception as error:
        logger.error({"error": f"ElasticSearch error; {error}"})

    REQUEST_INPROGRESS.set_to_current_time()
    #REQUEST_INPROGRESS.set(time.time())

    return response


def get_performance_metrics(run_id):
    response = _search_elastic_search(
        host="jaws-vm-1.jgi.lbl.gov",
        port="9200",
        api_key="VlAxTWhvMEItVUM1dkhYTGxkQ3k6Z2t3c1ZHZUxUd3lhWjdHSGdaYXJJdw==",
        index="performance_metrics",
        query={"match": {"jaws_run_id": int(run_id)}},
    )

    metrics = []
    try:
        for hit in response["hits"]["hits"]:
            metrics.append(hit["_source"])
    except Exception as error:
        status = 404
        if "error" in response and "status" in response:
            status = response["status"]
            error += json.dumps(response["error"], indent=2)
        logger.error(status, {"error": f"{error}"})
    return metrics


class Handler(http.server.BaseHTTPRequestHandler):
    def do_GET(self):
        self.send_response(200)
        self.end_headers()
        datetime_now = datetime.now()
        datetime_now = '{"time": "%s"}' % datetime_now
        self.wfile.write(bytes(json.dumps(metrics[0]), "UTF-8"))


if __name__ == "__main__":
    metrics = get_performance_metrics(run_id=66621)

    # start server that will be scraped by prometheus
    start_http_server(METRICS_PORT)

    # start server to query elasticsearch via the API
    server = http.server.HTTPServer(("localhost", APP_PORT), Handler)
    server.serve_forever()
